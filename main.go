package main

import (
	"fmt"
	"os"
	"strings"
    "regexp"
	"github.com/bitrise-io/go-utils/log"
	"gitlab.com/rudnicst/steps-post-jira-comment-with-build-detail/jira"
	"github.com/bitrise-tools/go-steputils/stepconf"
)

// Config ...
type Config struct {
	APIToken string `env:"api_token,required"`
	BaseURL  string `env:"base_url,required"`
	IssueKeys string `env:"issue_keys,required"`
	Message  string `env:"build_message,required"`
}

func main() {
	var cfg Config
	if err := stepconf.Parse(&cfg); err != nil {
		failf("Issue with input: %s", err)
	}

	stepconf.Print(cfg)
	fmt.Println()

	client := jira.NewClient(cfg.APIToken, cfg.BaseURL)
	issueKeys := strings.Split(cfg.IssueKeys, `|`)
	fmt.Println("Print issue keys:")
	fmt.Println(issueKeys)
    reg := regexp.MustCompile(`(?P<jira_id>(BMWVID|)-\d+)`)

	var comments []jira.Comment
	for _, issueKey := range issueKeys {
		comments = append(comments, jira.Comment{Content: cfg.Message, IssueKey: reg.FindString(issueKey)})
	}

	if err := client.PostIssueComments(comments); err != nil {
		failf("Posting comments failed with error: %s", err)
	}
}

func failf(format string, v ...interface{}) {
	log.Errorf(format, v...)
	os.Exit(1)
}
